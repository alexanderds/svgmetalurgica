from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    url(r'^sendemail$', views.email_send, name='emailsend'),
    url(r'^solutionlist$', views.solutions_list, name='solutionlist'),
    url(r'^solutiondetail/(?P<pk>\d+)$', views.solution_detail, name='solutiondetail'),
    url(r'^imagelist$', views.image_list, name='imagelist'),
    url(r'^imagedetail/(?P<pk>\d+)$', views.image_detail, name='imagedetail'),
    url(r'^more_info$', views.more_info, name='more_info' ),

]

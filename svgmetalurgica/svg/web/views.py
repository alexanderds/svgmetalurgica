from django.shortcuts import render, HttpResponse
from web.models import (Slide, Partner, Solutions, BasicInformation,
                        Image, Testimonials, Document, DocCategory, ReceiveEmail)
from django.core.mail import send_mail
from django.urls import reverse_lazy
import smtplib

# Create your views here.

def index(request):
    firtslide = Slide.objects.filter(slideactivit=True).first()
    slides = Slide.objects.filter(slideactivit=True).exclude(pk=firtslide.pk)
    count = slides.count()
    partners = Partner.objects.all()
    solutions = Solutions.objects.all()[:6]
    basicInformation = BasicInformation.objects.order_by('pk').last()
    image = Image.objects.all()[:6]
    testimonialsactive = Testimonials.objects.filter(active=True)[:2]
    testimonials = Testimonials.objects.filter(active=True)
    for item in testimonialsactive:
        testimonials = testimonials.exclude(pk=item.pk)

    testimonialslist = []
    quant = testimonials.count()
    if quant % 2 == 0 or quant == 1:
        if quant != 1: quant = quant/2
        for number in range(int(quant)):
            testimonialslist.append({'number': number + 1})
    else:
        quant = (quant + 1)/2
        for number in range(int(quant)):
            testimonialslist.append({'number': number + 1})

    slidelist = []
    for number in range(0, count):
        slidelist.append({'number': number + 1})

    return render(request, 'index.html', {'slides' : slides,
                                          'partners' : partners,
                                          'solutions' : solutions,
                                          'basic' : basicInformation,
                                          'images' : image,
                                          'firstslide' : firtslide,
                                          'count': slidelist,
                                          'testimonialsactive': testimonialsactive,
                                          'testminials': testimonials,
                                          'testimonialslist': testimonialslist})

def email_send(request):
    if request.method == 'POST':
        if request.is_ajax():
            name = request.POST['name']
            email =  request.POST['email']
            subject = request.POST['subject']
            message = request.POST['message']
            fromemail = BasicInformation.objects.all().last()

            ReceiveEmail.objects.create(
                name=name,
                subject=subject,
                email=email,
                message=message
            )

            send_mail(
                subject,
                "Enviado por: " + name + " Email: " + email + " Mensagem: " + message,
                'ti@svgmetalurgica.com.br',
                [fromemail.email],
                fail_silently=False,
            )

            return HttpResponse('Mensagem enviada')
    return reverse_lazy(index)

def solutions_list(request):
    solutions = Solutions.objects.all()
    return render(request, 'solution_list.html', {'solutions':solutions})

def solution_detail(request, pk):
    solution = Solutions.objects.get(pk=pk)

    return render(request, 'solution_detail.html',  {'solution': solution})

def image_list(request):
    images = Image.objects.all()

    return render(request, 'image_list.html', {'images': images})

def image_detail(request, pk):
    image = Image.objects.get(pk=pk)

    return render(request, 'image_detail.html', {'image': image})

def more_info(request):
    docs = Document.objects.all()
    categorys = DocCategory.objects.all()

    return render(request, 'more_informations.html', {'docs': docs, 'categorys': categorys})

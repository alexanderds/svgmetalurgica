# Generated by Django 2.0.6 on 2018-06-25 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0002_auto_20180621_0813'),
    ]

    operations = [
        migrations.CreateModel(
            name='Testimonials',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='Nome')),
                ('office', models.CharField(max_length=50, verbose_name='Cargo')),
                ('testimonials', models.CharField(max_length=50, verbose_name='Depoimento')),
                ('image', models.FileField(upload_to='imag/', verbose_name='Foto Perfil')),
            ],
            options={
                'verbose_name': 'Testemunho',
                'verbose_name_plural': 'Testemunhos',
            },
        ),
    ]

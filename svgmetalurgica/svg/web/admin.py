from django.contrib import admin
from .models import (ReceiveEmail, Slide, BasicInformation, Image, Solutions, Partner, Testimonials, Document, DocCategory)
import os
# Register your models here.


class ImageAdmin(admin.ModelAdmin):
    actions = ['delete_selected']

    def delete_selected(modeladmin, request, queryset):
        for obj in queryset:
            os.remove(os.path.join('media/', str(obj.image)))
            obj.delete()
    delete_selected.short_description = 'Deletar Selecionados'

class DeleteAdmin(admin.ModelAdmin):
    actions = ['delete_selected']

    def delete_selected(modeladmin, request, queryset):
        for obj in queryset:
            obj.delete()
    delete_selected.short_description = 'Deletar Selecionados'


admin.site.register(Slide, ImageAdmin)
admin.site.register(BasicInformation, DeleteAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Solutions, ImageAdmin)
admin.site.register(Partner, ImageAdmin)
admin.site.register(Testimonials, ImageAdmin)
admin.site.register(DocCategory, DeleteAdmin)
admin.site.register(Document, ImageAdmin)
admin.site.register(ReceiveEmail, DeleteAdmin)

from django.db import models
import os

class Slide(models.Model):
    slidetitle = models.CharField(max_length=50, verbose_name='Titulo')
    slidetext = models.TextField(verbose_name='Texto')
    image = models.FileField(upload_to='img/', verbose_name="Imagem (1900x720)")
    slideactivit = models.BooleanField('Ativo')
    entry_date = models.DateTimeField(verbose_name='Data de entrada', auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = 'Slide'
        verbose_name_plural = 'Slides'

    def delete(self, *args, **kwargs):
        os.remove(self.image)
        super(Slide, self).delete(*args, **kwargs)


    def __str__(self):
        return self.slidetitle

class BasicInformation(models.Model):
    mensage = models.TextField(verbose_name='Mensagem de bem vindo')
    mission = models.TextField(verbose_name='Missão')
    vision = models.TextField(verbose_name='Visão')
    values = models.TextField(verbose_name='Valores')
    address = models.CharField(max_length=100, verbose_name='Endereço')
    telnumber = models.CharField(max_length=14, verbose_name='Numero de Telefone')
    email = models.CharField(max_length=100, verbose_name='E-mail')

    class Meta:
        verbose_name = 'Informação basica'
        verbose_name_plural = 'Informações basicas'

    def __str__(self):
        return self.mensage

class Image(models.Model):
    imagetitle = models.CharField(max_length=30 ,verbose_name='Titulo da imagem')
    imagedescription = models.TextField('Descrição')
    image = models.FileField(upload_to='img/', verbose_name='Imagem')
    category = models.CharField(max_length=100, verbose_name='Categoria')

    def save(self, *args, **kwargs):
        self.category = self.category.replace(' ', '_')
        super(Image, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Imagem'
        verbose_name_plural = 'Imagens'

    def __str__(self):
        return self.imagetitle

class Solutions(models.Model):
    solutiontitle = models.CharField(max_length=30, verbose_name='Titulo')
    description = models.TextField(verbose_name='Descrição')
    image = models.FileField(upload_to='img/', verbose_name='Imagem')

    class Meta:
        verbose_name = 'Solução'
        verbose_name_plural = 'Soluções'

    def __str__(self):
        return self.solutiontitle

class Partner(models.Model):
    link = models.TextField(verbose_name='Link')
    name = models.CharField(max_length=100,verbose_name='Nome')
    image = models.FileField(upload_to='img/' ,verbose_name='Imagem')

    class Meta:
        verbose_name = 'Parceiro'
        verbose_name_plural = 'Parceiros'

    def __str__(self):
        return self.name

class Testimonials(models.Model):
    name = models.CharField(verbose_name='Nome', max_length=50)
    office = models.CharField(verbose_name='Cargo',max_length=50)
    testimonials = models.TextField(verbose_name='Depoimento')
    image = models.FileField(upload_to='imag/', verbose_name='Foto Perfil')
    active = models.BooleanField('Ativo')
    class Meta:
        verbose_name= 'Testemunho'
        verbose_name_plural = 'Testemunhos'

    def __str__(self):
        return self.name + ' - ' + self.office

class DocCategory(models.Model):
    name = models.CharField('Nome' ,max_length=50)

    class Meta:
        verbose_name = 'Categoria de Documento'
        verbose_name_plural = 'Categorias de Documento'

    def __str__(self):
        return self.name

class Document(models.Model):
    name = models.CharField('Nome do Arquivo', max_length=50)
    archive = models.FileField('Arquivo', upload_to='docs/', max_length=100)
    date_entry = models.DateField('Data', auto_now=True, auto_now_add=False)
    category = models.ForeignKey(DocCategory, related_name='DocXCategory', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Documento'
        verbose_name_plural = 'Documentos'

    def __str__(self):
        return  self.name

class ReceiveEmail(models.Model):
    name = models.CharField(max_length=50, verbose_name='Nome')
    subject = models.CharField(max_length=50, verbose_name='Assunto')
    email = models.CharField(max_length=250, verbose_name='E-mail')
    message = models.TextField('Mensagem')
    date_entry = models.DateTimeField('Data de entrada', auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = 'Email'
        verbose_name_plural = 'Emails'

    def __str__(self):
        return self.subject


